---
layout: handbook-page-toc
title: GitLab Marketing Team READMEs
description: "Learn more about working with various members of the marketing team"
---
## Marketing Team READMEs

- [Darren Murph's README (Head of Remote)](/handbook/marketing/readmes/dmurph/)
- [Jessica Reeder's README (Senior All-Remote Campaign Manager)](/handbook/marketing/readmes/jessicareeder/)
- [Laura Duggan's README (Frontend Engineer)](/handbook/marketing/readmes/laura-duggan.html)
- [Lauren Barker's README (Fullstack Engineer)](/handbook/marketing/readmes/lauren-barker.html)
- [Michael Preuss’ README (Director, Digital Experience)](/handbook/marketing/readmes/michael-preuss.html)
- [Nathan Dubord's README (Frontend Engineer)](/handbook/marketing/readmes/nathan-dubord.html)
- [Tyler Williams' README (Fullstack Engineer)](/handbook/marketing/readmes/tyler-williams.html)
- [Wil Spillane's README  (Head of Social)](/handbook/marketing/readmes/wspillane.html)
